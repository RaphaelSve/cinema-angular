import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Film } from '../models/film';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) { }

  getFilms(): Observable<Film[]> {
    // récupération via client Http Angular
    return this.http.get<Film[]>(`${environment.apiUrl}/films`);
  }

  getFilmsId(id: number): Observable<Film[]> {
    return this.http.get<Film[]>(`${environment.apiUrl}/films/${id}`);
  }

  createFilm(film: Film): Observable<Film> {
    return this.http.post<Film>(`${environment.apiUrl}/films`, film);
  }

  
  deleteFilm(salle: Film): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/films/${salle.id}`)
  }

  deleteFilmById(id: number): Observable<void> {
    return this.http.delete<void>(`${environment.apiUrl}/films/${id}`)
  }

  

}
