export interface Film {
    id : number
    overview: string, //Résumé du film
    popularity: number, // Note du film
    poster_path: string, //Image du film
    release_date: string,
    title: string,
    room : string
}