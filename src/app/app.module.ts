import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmPageComponent } from './film-page/film-page.component';
import { FilmCardComponent } from './film-page/film-card/film-card.component';
import { HttpClientModule } from '@angular/common/http';
import { ClientPageComponent } from './client-page/client-page.component';
import { ClientCardComponent } from './client-page/client-card/client-card.component';
import { ClientCreateComponent } from './client-page/client-create/client-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SallePageComponent } from './salle-page/salle-page.component';
import { SalleCardComponent } from './salle-page/salle-card/salle-card.component';
import { SalleCreateComponent } from './salle-page/salle-create/salle-create.component';
import { FilmCreateComponent } from './film-page/film-create/film-create.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SalleFilmComponent } from './salle-page/salle-film/salle-film.component';


@NgModule({
  declarations: [
    AppComponent,
    FilmPageComponent,
    FilmCardComponent,
    ClientPageComponent,
    ClientCardComponent,
    ClientCreateComponent,
    SallePageComponent,
    SalleCardComponent,
    SalleCreateComponent,
    FilmCreateComponent,
    NotFoundComponent,
    SalleFilmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
