import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FilmService } from 'src/app/shared/services/film.service';

@Component({
  selector: 'app-film-create',
  templateUrl: './film-create.component.html',
  styleUrls: ['./film-create.component.css']
})
export class FilmCreateComponent implements OnInit {

  filmForm: FormGroup;

  ngOnInit(): void {
  }

  constructor(private filmService: FilmService, private router: Router) {
    // création du formulaire à la création du composant
    this.filmForm = new FormGroup({
      // un formControl par input
      title: new FormControl("", [Validators.required, Validators.minLength(5)]),
      overview: new FormControl("", [Validators.required, Validators.minLength(5)]),
      release_date: new FormControl("", [Validators.required, Validators.minLength(5)]),
      poster_path: new FormControl("", [Validators.required, Validators.minLength(5)]),
    })}

  sendForm(): void {
    // si le formulaire est valide
    if (this.filmForm.valid) {
      // j'envois mon utilisateur à l'API
      this.filmService.createFilm(this.filmForm.value)
        // si on ne se sert pas de la reponse on mets un "_" en param du callback
        .subscribe((_) => {
          this.router.navigate(['../films']);
        })
    }
    console.log(this.filmForm);
  }

}
