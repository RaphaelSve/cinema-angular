import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Film } from '../shared/models/film';
import { FilmService } from '../shared/services/film.service';

@Component({
  selector: 'app-film-page',
  templateUrl: './film-page.component.html',
  styleUrls: ['./film-page.component.css']
})
export class FilmPageComponent implements OnInit {

  id?: number = undefined;
  films : Film[] = []

  constructor(private route : ActivatedRoute, private filmService : FilmService, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    // récupération en asynchrone des params de l'url
    this.route.params.subscribe((params) => {
      console.log(params);
    })
    // appel du service
    this.refreshFilms();
  }

  refreshFilms(): Subscription {
    return this.filmService.getFilms()
      // souscription aux changements de l'observable
      .subscribe(
        // dés qu'il y a une reponse
        (films: Film[]) => {
          // j'assigne les utilisateurs récupére au tableau du composant
          this.films = films;
        }
      );
  }

  newFilm(): void {
    this.router.navigate(['../films/new']);
  }

}
