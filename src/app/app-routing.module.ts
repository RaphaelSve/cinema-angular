import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientCreateComponent } from './client-page/client-create/client-create.component';
import { ClientPageComponent } from './client-page/client-page.component';
import { FilmCreateComponent } from './film-page/film-create/film-create.component';
import { FilmPageComponent } from './film-page/film-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SalleCreateComponent } from './salle-page/salle-create/salle-create.component';
import { SalleFilmComponent } from './salle-page/salle-film/salle-film.component';
import { SallePageComponent } from './salle-page/salle-page.component';

const routes: Routes = [
  { path: "", redirectTo : "films", pathMatch : "full"},
  { path: "films",

  children : [
    { path: "", component: FilmPageComponent },
    { path: "new", component: FilmCreateComponent },
    { path: ":id", component: FilmPageComponent, pathMatch : "full"}
  ]
  },

  { path: "clients",

  children : [
    { path: "", component: ClientPageComponent },
    { path: "new", component: ClientCreateComponent},
    { path: ":id", component: ClientPageComponent},
  ]
  },

  { path: "salles",

  children : [
    { path: "", component: SallePageComponent },
    { path: "new", component: SalleCreateComponent},
    { path: "newfilm", component: SalleFilmComponent},
    { path: ":id", component: SallePageComponent},
  ]
  },

  { path: "**", component: NotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
