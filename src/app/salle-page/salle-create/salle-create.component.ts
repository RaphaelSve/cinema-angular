import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SalleService } from 'src/app/shared/services/salle.service';

@Component({
  selector: 'app-salle-create',
  templateUrl: './salle-create.component.html',
  styleUrls: ['./salle-create.component.css']
})
export class SalleCreateComponent implements OnInit {

  salleForm: FormGroup;

  ngOnInit(): void {
  }


  constructor(private salleService: SalleService, private router: Router) {
    // création du formulaire à la création du composant
    this.salleForm = new FormGroup({
      // un formControl par input
      roomName: new FormControl("", [Validators.required, Validators.minLength(5)]),
    })}

  sendForm(): void {
    // si le formulaire est valide
    if (this.salleForm.valid) {
      // j'envois mon utilisateur à l'API
      this.salleService.createSalle(this.salleForm.value)
        // si on ne se sert pas de la reponse on mets un "_" en param du callback
        .subscribe((_) => {
          this.router.navigate(['../salles']);
        })
    }
    console.log(this.salleForm);
  }

}
