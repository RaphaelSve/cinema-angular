import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Film } from 'src/app/shared/models/film';
import { Salle } from 'src/app/shared/models/salle';
import { FilmService } from 'src/app/shared/services/film.service';

@Component({
  selector: 'app-salle-film',
  templateUrl: './salle-film.component.html',
  styleUrls: ['./salle-film.component.css']
})
export class SalleFilmComponent implements OnInit {

  id?: number = undefined;
  films : Film[] = [];
  @Input() salle!: Salle;

  constructor(private route : ActivatedRoute, private filmService : FilmService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    // récupération en asynchrone des params de l'url
    this.route.params.subscribe((params) => {
      console.log(params);
    })
    // appel du service
    this.refreshFilms();
  }

  refreshFilms(): Subscription {
    return this.filmService.getFilms()
      // souscription aux changements de l'observable
      .subscribe(
        // dés qu'il y a une reponse
        (films: Film[]) => {
          // j'assigne les utilisateurs récupére au tableau du composant
          this.films = films;
        }
      );
  }
}
