import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalleFilmComponent } from './salle-film.component';

describe('SalleFilmComponent', () => {
  let component: SalleFilmComponent;
  let fixture: ComponentFixture<SalleFilmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalleFilmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalleFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
