import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Salle } from 'src/app/shared/models/salle';
import { SalleService } from 'src/app/shared/services/salle.service';

@Component({
  selector: 'app-salle-card',
  templateUrl: './salle-card.component.html',
  styleUrls: ['./salle-card.component.css']
})
export class SalleCardComponent implements OnInit {

  @Input() salle!: Salle;
  // Output: remonté d'information vers le composant parent
  @Output() deleteEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor( private salleService: SalleService, private router: Router) { }

  ngOnInit(): void {
  }

  deleteSalle(): void {
    if (this.salle.id) {
      this.salleService.deleteSalleById(this.salle.id)
        .subscribe((_) => {
          // this.router.navigate(['../status'])
          this.deleteEmitter.emit();
        })
    }
  }


  newFilm(): void {
    this.router.navigate(['../salles/newfilm']);
  }

}
