import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Salle } from '../shared/models/salle';
import { SalleService } from '../shared/services/salle.service';

@Component({
  selector: 'app-salle-page',
  templateUrl: './salle-page.component.html',
  styleUrls: ['./salle-page.component.css']
})
export class SallePageComponent implements OnInit {


  id? : number = undefined;
  salles : Salle[] = [];

  constructor(private route : ActivatedRoute, private salleService : SalleService, private router: Router) { }


  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    // récupération en asynchrone des params de l'url
    this.route.params.subscribe((params) => {
      console.log(params);
    })
    // appel du service
    this.refreshSalles();
  }

  refreshSalles(): Subscription {
    return this.salleService.getSalles()
      // souscription aux changements de l'observable
      .subscribe(
        // dés qu'il y a une reponse
        (salles: Salle[]) => {
          // j'assigne les utilisateurs récupére au tableau du composant
          this.salles = salles;
        }
      );
  }

  newSalle(): void {
    this.router.navigate(['../salles/new']);
  }

}
