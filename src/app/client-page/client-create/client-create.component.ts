import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/shared/services/client.service';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {

  clientForm: FormGroup;

  ngOnInit(): void {
  }


  constructor(private clientService: ClientService, private router: Router) {
    // création du formulaire à la création du composant
    this.clientForm = new FormGroup({
      // un formControl par input
      name: new FormControl("", [Validators.required, Validators.minLength(5)]),
      firstname: new FormControl("", [Validators.required, Validators.minLength(5)]),
      email: new FormControl("", Validators.email)
    })}

  sendForm(): void {
    // si le formulaire est valide
    if (this.clientForm.valid) {
      // j'envois mon utilisateur à l'API
      this.clientService.createClient(this.clientForm.value)
        // si on ne se sert pas de la reponse on mets un "_" en param du callback
        .subscribe((_) => {
          this.router.navigate(['../clients']);
        })
    }
    console.log(this.clientForm);
  }
}
